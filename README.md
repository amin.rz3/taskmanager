# TaskManager

TodoList Project - Php,Ajax,Mysql

## Demo

![](Demo/demo.gif)

## Getting Started

Create Database And Import Sql File From db-backup Folder

You Need Change Database information From bootstrap/config.php File

Also You Need Change Infromation From bootstrap/constants.php

## bootstrap/constants.php File Help

For Change Title Site Just Edit This Define:

```
 define('SITE_TITLE','TaskManager Project');
```

You Must Be Edit BASE_URL And BASE_PATH For Run This Project:

```
 define('BASE_URL','http://localhost/Taskmanager/');
 define('BASE_PATH','C:/xampp/htdocs/Taskmanager/');
```

For Reverse Show Tasks Just Set Define REVERSE_TASKS As latest_first:

```
 define('REVERSE_TASKS','latest_first');
```

You Can Set the number of tasks displayed on each page:

```
 define('TASKS_NUM',8);
```

## Running the tests

Xampp - Php Version 7.4.4