<?php
include 'bootstrap/init.php';

if(isset($_GET['logout'])){
    logOut();
}

if(!isLogin()){
    header('Location: '.site_url('auth.php'));
}

$userData = $_SESSION['Login'];

if(isset($_GET['delete_folder']) && is_numeric($_GET['delete_folder'])){
    deleteFolder($_GET['delete_folder']);
}

if(isset($_GET['delete_task']) && is_numeric($_GET['delete_task'])){
    deleteTask($_GET['delete_task']);
}

$folders = getFolders();
$tasks = getTasks();
$pageNum = $_GET['page'] ?? 1;
$number_of_pages = ceil((count(getNumOfTasks())-1)/TASKS_NUM);

if(($pageNum-1)<=0){
    if (isset($_GET['folder_id'])){
        $blink = '?folder_id='.$_GET['folder_id'].'&page=1';
    }else{
        $blink = '?page=1';
    }
}else{
    if (isset($_GET['folder_id'])){
        $blink = '?folder_id='.$_GET['folder_id'].'&page='.($pageNum-1);
    }else{
        $blink = '?page='.($pageNum-1);
    }
}

if(($pageNum+1)>$number_of_pages){
    if (isset($_GET['folder_id'])){
        $nlink = '?folder_id='.$_GET['folder_id'].'&page=1';
    }else{
        $nlink = '?page=1';
    }
}else{
    if (isset($_GET['folder_id'])){
        $nlink = '?folder_id='.$_GET['folder_id'].'&page='.($pageNum+1);
    }else{
        $nlink = '?page='.($pageNum+1);
    }
}

include 'tpl/tpl-index.php';