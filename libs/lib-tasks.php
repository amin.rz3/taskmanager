<?php defined('BASE_PATH') OR die("Permission Denied");

function deleteFolder($id)
{
    global $pdo;
    $sql = "delete from folders where id = $id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->rowCount();
}

function deleteTask($id)
{
    global $pdo;
    $sql = "delete from tasks where id = $id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->rowCount();
}

function getFolders()
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "select * from folders where user_id = $current_user_id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $records;
}

function addFolder($data)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "INSERT INTO `folders` (name,user_id) VALUES (:folderName,:user_id)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':folderName' => $data, ':user_id' => $current_user_id]);
    if ($stmt->rowCount() == 1) {
        return ['Result' => '200', 'FolderName' => $data, 'FolderId' => $pdo->lastInsertId(), 'UserId' => $current_user_id];
    } else {
        return ['Result' => '400', 'FolderName' => '', 'FolderId' => '', 'UserId' => ''];
    }
}

function getTasks()
{
    global $pdo;

    $page = $_GET['page'] ?? 1;
    $pageCondition = '';

    $folder_id = $_GET['folder_id'] ?? null;
    $folderCondition = '';
    if (isset($folder_id) && is_numeric($folder_id)) {
        $folderCondition = "and folder_id=$folder_id";
    }

    if (isset($page) && is_numeric($page)) {
        $this_page_first_result = ($page-1)*TASKS_NUM;
        $pageCondition = 'LIMIT '.$this_page_first_result.','.TASKS_NUM;
    }

    $current_user_id = getCurrentUserId();

    $ReverseCondition = REVERSE_TASKS == 'latest_first' ? 'ORDER BY id DESC' : '';
    $sql = "select * from tasks where user_id = $current_user_id $folderCondition $ReverseCondition $pageCondition";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $records;
}

function getNumOfTasks(){
    global $pdo;
    $folder_id = $_GET['folder_id'] ?? null;
    $folderCondition = '';
    if (isset($folder_id) && is_numeric($folder_id)) {
        $folderCondition = "and folder_id=$folder_id";
    }

    $current_user_id = getCurrentUserId();

    $ReverseCondition = REVERSE_TASKS == 'latest_first' ? 'ORDER BY id DESC' : '';
    $sql = "select * from tasks where user_id = $current_user_id $folderCondition $ReverseCondition";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $records;
}

function addTask($folderId, $TaskName)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "INSERT INTO `tasks` (title,user_id,folder_id) VALUES (:title,:user_id,:folder_id)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':title' => $TaskName, ':user_id' => $current_user_id, ':folder_id' => $folderId]);
    if ($stmt->rowCount() == 1) {
        return ['Result' => '200', 'title' => $TaskName, 'FolderId' => $folderId, 'UserId' => $current_user_id];
    } else {
        return ['Result' => '400', 'title' => '', 'FolderId' => '', 'UserId' => ''];
    }
}

function switchIsDone($data)
{
    global $pdo;
    $current_user_id = getCurrentUserId();
    $sql = "Update `tasks` set is_done=1-is_done where user_id = :user_id and id = :taskId";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':user_id' => $current_user_id, ':taskId' => $data]);
    if ($stmt->rowCount() == 1) {
        return getTasksWithId($data);
    } else {
        return '0';
    }
}

function getTasksWithId($data)
{
    global $pdo;
    $sql = "select * from tasks where id = $data";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $records;
}

function Search($query)
{
    global $pdo;
    $folder_id = $query['folderId']==0 ? null : $query['folderId'];
    $current_user_id = getCurrentUserId();
    $folderCondition = "";
    if (isset($folder_id) && is_numeric($folder_id)) {
        $folderCondition = "and folder_id=$folder_id";
    }
    $ReverseCondition = REVERSE_TASKS == 'latest_first' ? 'ORDER BY id DESC' : '';
    if (isset($query['query'])) {
        $sql = "select * from tasks where user_id = :user_id $folderCondition and title like :query $ReverseCondition";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':user_id' => $current_user_id, ':query'=>'%'.$query['query'].'%']);
    } else {
        $sql = "select * from tasks where user_id = :user_id $folderCondition $ReverseCondition";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':user_id' => $current_user_id]);
    }
    $records = $stmt->fetchAll(PDO::FETCH_OBJ);

    $output = '';

    if(count($records)){
        foreach ($records as $data){
            $id = $data->id;
            $title = $data->title;
            $created_at = $data->created_at;
            if ($data->is_done) {
                $output .= "
    <li class='task-item checked'>
                                    <i data-taskId='$id' class='isDone clickable fa fa-check-square-o'></i>
                                    <span>$title</span>
                                    <div class='info''>
                                        <a href='?delete_task=$id'><i class='fa fa-trash-o clickable' style='float: right;'></i></a>
                                        <span>Created at $created_at</span>
                                    </div>
                                </li>
    ";
            }else{
                $output .= "
    <li class='task-item'>
                                    <i data-taskId='$id' class='isDone clickable fa fa-square-o'></i>
                                    <span>$title</span>
                                    <div class='info''>
                                        <a href='?delete_task=$id'><i class='fa fa-trash-o clickable' style='float: right;'></i></a>
                                        <span>Created at $created_at</span>
                                    </div>
                                </li>
    ";
            }
        }
    }else{
        $output = '
                             <li>
                                <span>No Task Here !</span>
                            </li>
        ';
    }

    return $output;
}





