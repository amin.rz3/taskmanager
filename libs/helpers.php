<?php defined('BASE_PATH') OR die("Permission Denied");

function diePage($msg){
    echo "<div style='background:#e6dbdb;width: 60%;margin: 30px;padding: 20px;border-radius: 5px;font-family: sans-serif;'>$msg</div>";
    die();
}

function isAjaxRequest(){
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
        return true;
    }
    return false;
}

function site_url($url = ''){
    return BASE_URL . $url;
}