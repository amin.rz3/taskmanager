<?php defined('BASE_PATH') OR die("Permission Denied");

function getCurrentUserId()
{
    return $_SESSION['Login']->id ?? 0;
}

function isLogin()
{
    return isset($_SESSION['Login']) ? true : false;
}

function logOut(){
    unset($_SESSION['Login']);
}

function Login($params){
    $email = $params['email'];
    $password = $params['password'];
    $data = getUserDetail($email);

    if(is_null($data)){
        return "Error";
    }

    if(password_verify($password,$data->password)){
        $data->image = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) );
        $_SESSION['Login'] = $data;
        return "LoginOk";
    }

    return "Error";
}

function getUserDetail($email){
    global $pdo;
    $sql = "select * from users where email = :email";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':email'=>$email]);
    $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $records[0] ?? null;
}

function Register($params){
    $name = $params['name'];
    $email = $params['email'];
    $password = $params['password'];

    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
        return "Email Not Valid";
    }

    if(strlen($password)<5){
        return "Password not Strong";
    }

    $password = password_hash($password,PASSWORD_BCRYPT);

    global $pdo;
    $sql = "INSERT INTO `users` (name,email,password) VALUES (:name,:email,:password)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':name'=>$name,':email'=>$email,':password'=>$password]);
    return $stmt->rowCount();
}
