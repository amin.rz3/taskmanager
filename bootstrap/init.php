<?php
session_start();
include 'constants.php';
include 'config.php';
include BASE_PATH . 'libs/helpers.php';


$dsn = "mysql:dbname=$database_config->db;host=$database_config->host";
$user = $database_config->user;
$password = $database_config->password;

try {
    $pdo = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    diePage($e);
}

//echo "Connection Successfully";


include BASE_PATH . 'libs/lib-auth.php';
include BASE_PATH . 'libs/lib-tasks.php';