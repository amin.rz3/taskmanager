<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= SITE_TITLE ?></title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/styleAlert.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="page">
    <div class="pageHeader">
        <div class="title">Dashboard</div>
        <div class="userPanel">
            <a href="<?= site_url('?logout=1') ?>"><i class="fa fa-sign-out fa-lg" style="text-decoration: none;color:#fff;"></i></a>
            <span class="username"><?= $userData->name ?></span><img
             src="<?= $userData->image ?>" width="40" height="40"/></div>
    </div>
    <div class="main">
        <div class="nav">
            <div class="searchbox">
                <div><i class="fa fa-search"></i>
                    <input id="search_task" type="search" placeholder="Search"/>
                </div>
            </div>
            <div class="menu">
                <div class="title">Folders</div>
                <ul class="folder-list">
                    <a href="<?= BASE_URL ?>" style="text-decoration: none; color: #0b3749;"><li class="<?= isset($_GET['folder_id']) ? '' : 'active' ;?>"><i class="fa <?= isset($_GET['folder_id']) ? 'fa-folder' : 'fa-folder-open' ;?>"></i>All</li></a>
                    <?php foreach ($folders as $folder): ?>
                        <li class="<?= ($_GET['folder_id'] == $folder->id) ? 'active' : '' ;?>">
                            <a href="?folder_id=<?= $folder->id ?>"><i class="fa <?= ($_GET['folder_id'] == $folder->id) ? 'fa-folder-open' : 'fa-folder' ;?>"></i><?= $folder->name ?></a>
                            <a href="?delete_folder=<?= $folder->id ?>"><i class="fa fa-trash-o clickable" style="float: right;" onclick="return confirm('Are You Sure Delete <?=$folder->name?> Folder?')"></i></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div>
                <input type="text" id="addFolderInput" placeholder="Add New Folder"
                       style="width: 65%; margin-left: 3%"/>
                <button id="addFolderBtn" class="btn clickable">+</button>
            </div>

        </div>
        <div class="view">
            <div class="viewHeader" style="visibility: <?= isset($_GET['folder_id']) ? 'visible' : 'hidden' ?>;">
                <div class="title">
                    <input type="text" id="addTaskInput" placeholder="Add New Task" style="width: 100%;line-height: 33px;padding-left: 15px;"/>
                </div>
                <div class="functions">
                    <div id="addTaskButton" class="button active">Add New Task</div>
                </div>
            </div>
            <div class="content" style="top: <?= isset($_GET['folder_id']) ? '70px' : '0px' ?>;">
                <div class="list">
                    <div class="title">Manage Tasks</div>
                    <ul id="tasls-list">
                        <?php if(sizeof($tasks)): ?>
                            <?php foreach ($tasks as $task): ?>
                                <li class="task-item <?= $task->is_done ? 'checked' : '' ;?>">
                                    <i data-taskId="<?= $task->id ?>" class="isDone clickable fa <?= $task->is_done ? 'fa-check-square-o' : 'fa-square-o' ;?>"></i>
                                    <span><?= $task->title ?></span>
                                    <div class="info">
                                        <a href="?delete_task=<?= $task->id ?>"><i class="fa fa-trash-o clickable" style="float: right;" onclick="return confirm('Are You Sure Delete <?=$task->title?> Item?')"></i></a>
                                        <span>Created at <?= $task->created_at ?></span>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <li>
                                <span>No Task Here !</span>
                            </li>
                        <?php endif; ?>
                    </ul>

                </div>

                <ul class="pagination modal-1">
                    <li><a href="<?= $blink ?? '#' ?>" class="prev">&laquo</a></li>
                    <?php for ($page=1;$page<=$number_of_pages;$page++): ?>
                        <li><a href="<?= isset($_GET['folder_id']) ? '?folder_id='.$_GET['folder_id'].'&page='.$page : '?page='.$page ?>" class="<?= $pageNum==$page ? 'active' : '' ;?>"><?= $page ?></a></li>
                    <?php endfor; ?>
                    <li><a href="<?= $nlink ?? '#' ?>" class="next">&raquo;</a></li>
                </ul>

            </div>
        </div>
    </div>
</div>
<div id="alert-container"></div>

<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="assets/js/script.js"></script>
<script>
    $(document).ready(function () {

        function load_data(query)
        {
            $.ajax({
                url:"process/ajaxHandler.php",
                method:"POST",
                data:{action: "search",query:query,folderId: <?= $_GET['folder_id'] ?? 0 ?>},
                success:function(data)
                {
                    $('#tasls-list').html(data);
                }
            });
        }

        $('#search_task').keyup(function(){
            var search = $(this).val();
            if(search != '')
            {
                load_data(search);
            }
            else
            {
                load_data();
            }
        });

        $('.isDone').click(function (e) {
            var tid = $(this).attr('data-taskId');
            $.ajax({
                url: "process/ajaxHandler.php",
                method: "post",
                data: {action: "switchIdDone", taskId: tid},
                success: function (response) {
                    response = JSON.parse(response);
                    if(response.Result!='0'){
                        location.reload();
                    }
                }
            });
        });

        $('#addFolderBtn').click(function (e) {
            var input = $('#addFolderInput');
            $.ajax({
                url: "process/ajaxHandler.php",
                method: "post",
                data: {action: "addFolder", folderName: input.val()},
                success: function (response) {
                    response = JSON.parse(response);
                    if(response.Result=="200"){
                        $('<li><a href="?folder_id='+response.FolderId+'"><i class="fa fa-folder"></i>'+response.FolderName+'</a><a href="?delete_folder='+response.FolderId+'"><i class="fa fa-trash-o clickable" style="float: right;" onclick="return confirm(\'Are You Sure Delete '+response.FolderName+' Folder?\')"></i></a></li>').appendTo('ul.folder-list');
                        let alerts = document.getElementById("alert-container");
                        if (alerts.childElementCount < 2) {
                            // Create alert box
                            let alertBox = document.createElement("div");
                            alertBox.classList.add("alert-msg", "slide-in");

                            // Add message to alert box
                            let alertMsg = document.createTextNode("Create Folder Successfully");
                            alertBox.appendChild(alertMsg);

                            // Add alert box to parent
                            alerts.insertBefore(alertBox, alerts.childNodes[0]);

                            // Remove last alert box
                            alerts.childNodes[1].classList.add("slide-out");
                            setTimeout(function() {
                                alerts.removeChild(alerts.lastChild);
                            }, 600);
                        }
                    }else {
                        let alerts = document.getElementById("alert-container");
                        if (alerts.childElementCount < 2) {
                            // Create alert box
                            let alertBox = document.createElement("div");
                            alertBox.classList.add("alert-msg-error", "slide-in");

                            // Add message to alert box
                            let alertMsg = document.createTextNode("The folder name must be more than 3 characters");
                            alertBox.appendChild(alertMsg);

                            // Add alert box to parent
                            alerts.insertBefore(alertBox, alerts.childNodes[0]);

                            // Remove last alert box
                            alerts.childNodes[1].classList.add("slide-out");
                            setTimeout(function() {
                                alerts.removeChild(alerts.lastChild);
                            }, 600);
                        }
                    }
                },
            });
        });

        $('#addTaskButton').click(function (e) {
            var input = $('#addTaskInput');
            $.ajax({
                url: "process/ajaxHandler.php",
                method: "post",
                data: {action: "addTask", folderId: "<?= $_GET['folder_id'] ?? 0 ?>",taskName: input.val()},
                success: function (response) {
                    response = JSON.parse(response);
                    if(response.Result=="200"){
                        location.reload();
                    }else if(response.Result=="400") {
                        let alerts = document.getElementById("alert-container");
                        if (alerts.childElementCount < 2) {
                            // Create alert box
                            let alertBox = document.createElement("div");
                            alertBox.classList.add("alert-msg-error", "slide-in");

                            // Add message to alert box
                            let alertMsg = document.createTextNode("The Task name must be more than 3 characters");
                            alertBox.appendChild(alertMsg);

                            // Add alert box to parent
                            alerts.insertBefore(alertBox, alerts.childNodes[0]);

                            // Remove last alert box
                            alerts.childNodes[1].classList.add("slide-out");
                            setTimeout(function() {
                                alerts.removeChild(alerts.lastChild);
                            }, 600);
                        }
                    }else{
                        let alerts = document.getElementById("alert-container");
                        if (alerts.childElementCount < 2) {
                            // Create alert box
                            let alertBox = document.createElement("div");
                            alertBox.classList.add("alert-msg-error", "slide-in");

                            // Add message to alert box
                            let alertMsg = document.createTextNode("Please Select Folder To Create Task !");
                            alertBox.appendChild(alertMsg);

                            // Add alert box to parent
                            alerts.insertBefore(alertBox, alerts.childNodes[0]);

                            // Remove last alert box
                            alerts.childNodes[1].classList.add("slide-out");
                            setTimeout(function() {
                                alerts.removeChild(alerts.lastChild);
                            }, 600);
                        }
                    }
                },
            });
        });

        $('#addTaskInput').on('keypress',function(e) {
            if(e.which == 13) {
                var input = $('#addTaskInput');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    method: "post",
                    data: {action: "addTask", folderId: "<?= $_GET['folder_id'] ?? 0 ?>",taskName: input.val()},
                    success: function (response) {
                        response = JSON.parse(response);
                        if(response.Result=="200"){
                            location.reload();
                        }else if(response.Result=="400") {
                            let alerts = document.getElementById("alert-container");
                            if (alerts.childElementCount < 2) {
                                // Create alert box
                                let alertBox = document.createElement("div");
                                alertBox.classList.add("alert-msg-error", "slide-in");

                                // Add message to alert box
                                let alertMsg = document.createTextNode("The Task name must be more than 3 characters");
                                alertBox.appendChild(alertMsg);

                                // Add alert box to parent
                                alerts.insertBefore(alertBox, alerts.childNodes[0]);

                                // Remove last alert box
                                alerts.childNodes[1].classList.add("slide-out");
                                setTimeout(function() {
                                    alerts.removeChild(alerts.lastChild);
                                }, 600);
                            }
                        }else{
                            let alerts = document.getElementById("alert-container");
                            if (alerts.childElementCount < 2) {
                                // Create alert box
                                let alertBox = document.createElement("div");
                                alertBox.classList.add("alert-msg-error", "slide-in");

                                // Add message to alert box
                                let alertMsg = document.createTextNode("Please Select Folder To Create Task !");
                                alertBox.appendChild(alertMsg);

                                // Add alert box to parent
                                alerts.insertBefore(alertBox, alerts.childNodes[0]);

                                // Remove last alert box
                                alerts.childNodes[1].classList.add("slide-out");
                                setTimeout(function() {
                                    alerts.removeChild(alerts.lastChild);
                                }, 600);
                            }
                        }
                    },
                });
            }
        });

        $('#addTaskInput').focus();
    });
</script>
</body>
</html>
