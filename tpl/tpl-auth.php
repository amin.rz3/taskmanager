<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TaskManager Login</title>
    <link rel="stylesheet" href="assets/css/auth.css">
    <link rel="stylesheet" href="assets/css/styleAlert.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div id="background">
    <div id="panel-box">
        <div class="panel">
            <div class="auth-form on" id="login">
                <div id="form-title">Log In</div>
                <form action="<?= site_url('auth.php?action=login') ?>" method="POST">
                    <input name="email" type="text" required="required" placeholder="Email"/>
                    <input name="password" type="password" required="required" placeholder="Password"/>
                    <button type="Submit">Log In</button>
                </form>
            </div>
            <div class="auth-form" id="signup">
                <div id="form-title">Register</div>
                <form action="<?= site_url('auth.php?action=register') ?>" method="POST">
                    <input name="name" type="text" required="required" placeholder="Name"/>
                    <input name="email" type="text" required="required" placeholder="Email"/>
                    <input name="password" type="password" required="required" placeholder="Password"/>
                    <button type="Submit">Sign Up</button>
                </form>
            </div>
        </div>
        <div class="panel">
            <div id="switch">Sign Up</div>
            <div id="image-overlay"></div>
            <div id="image-side"></div>
        </div>
    </div>
    <div id="alert-container"></div>
</div>
<!-- partial -->
<script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
<script>

    $(document).ready(function () {

        var ErrorMessage = function($message){
            let alerts = document.getElementById("alert-container");
            if (alerts.childElementCount < 2) {
                // Create alert box
                let alertBox = document.createElement("div");
                alertBox.classList.add("alert-msg-error", "slide-in");

                // Add message to alert box
                let alertMsg = document.createTextNode($message);
                alertBox.appendChild(alertMsg);

                // Add alert box to parent
                alerts.insertBefore(alertBox, alerts.childNodes[0]);

                // Remove last alert box
                alerts.childNodes[1].classList.add("slide-out");
                setTimeout(function () {
                    alerts.removeChild(alerts.lastChild);
                }, 600);
            }
        };

        var OkMessage = function($message){
            let alerts = document.getElementById("alert-container");
            if (alerts.childElementCount < 2) {
                // Create alert box
                let alertBox = document.createElement("div");
                alertBox.classList.add("alert-msg", "slide-in");

                // Add message to alert box
                let alertMsg = document.createTextNode($message);
                alertBox.appendChild(alertMsg);

                // Add alert box to parent
                alerts.insertBefore(alertBox, alerts.childNodes[0]);

                // Remove last alert box
                alerts.childNodes[1].classList.add("slide-out");
                setTimeout(function () {
                    alerts.removeChild(alerts.lastChild);
                }, 600);
            }
        };

        $('#switch').click(function () {
            $(this).text(function (i, text) {
                return text === "Sign Up" ? "Log In" : "Sign Up";
            });
            $('#login').toggleClass("on");
            $('#signup').toggleClass("on");
            $(this).toggleClass("two");
            $('#background').toggleClass("two");
            $('#image-overlay').toggleClass("two");
        });

        var data = '<?= $data ?>';
        if (data == "Email Not Valid") {
            $('#switch').click();
            ErrorMessage("Please Enter Valid Email Address");
        }else if(data == "Password not Strong"){
            $('#switch').click();
            ErrorMessage("Please enter a strong password");
        }else if(data == "1"){
            OkMessage("Registered Successfull Please Login !");
        }else if(data=="Error"){
            ErrorMessage("Email or password is incorrect !");
        }
    });


</script>

</body>
</html>
