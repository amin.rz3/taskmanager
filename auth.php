<?php
include 'bootstrap/init.php';

$data = "";
if($_SERVER['REQUEST_METHOD']=='POST'){
    $action = $_GET['action'];
    $params = $_POST;
    if($action=="register"){
        $data = Register($params);
    }elseif($action=="login"){
        $data = Login($params);
        if($data=="LoginOk"){
            header('Location: '.site_url());
            die();
        }
    }
}

include 'tpl/tpl-auth.php';