<?php
include_once '../bootstrap/init.php';

if (!isAjaxRequest()) {
    diePage("Invalid Request");
}

if (!isset($_POST['action']) || empty($_POST['action'])) {
    diePage("Invalid Request Action");
}

switch ($_POST['action']) {

    case "switchIdDone":
        if (!isset($_POST['taskId']) && empty($_POST['taskId'])) {
            die();
        }
        echo json_encode(switchIsDone($_POST['taskId']));
        break;

    case "addFolder":
        if (isset($_POST['folderName']) && strlen($_POST['folderName']) > 3) {
            echo json_encode(addFolder($_POST['folderName']));
        } else {
            echo json_encode(['Result' => '400']);
        }
        break;

    case "addTask":

        if (!isset($_POST['folderId']) || empty($_POST['folderId'])) {
            echo json_encode(['Result' => '300']);
            die();
        }

        if (!isset($_POST['taskName']) || strlen($_POST['taskName']) < 3) {
            echo json_encode(['Result' => '400']);
            die();
        }

        echo json_encode(addTask($_POST['folderId'], $_POST['taskName']));

        break;
    case "search":
        echo Search($_POST);
        break;
    default:
        diePage("Invalid Request Action");
        break;
}
